SAMPLES={}
SAMPLES['UT1_1']=['RID100352','RID100651']
SAMPLES['UT2_1']='RID100353','RID100652'
SAMPLES['LPS1_1']='RID100363','RID100662'
SAMPLES['LPS2_1']='RID100364','RID100663','RID100909'

SAMPLES['UT1_2']='RID100461','RID100749','RID100995' 
SAMPLES['UT2_2']='RID100462','RID100750','RID100996'
SAMPLES['LPS1_2']='RID100469','RID100757','RID101003'
SAMPLES['LPS2_2']='RID100470','RID100758','RID101004'

COUNTS1=['UT1_1', 'UT2_1', 'LPS1_1', 'LPS2_1']
COUNTS2=['UT1_2', 'UT2_2', 'LPS1_2', 'LPS2_2']

PRJ_ROOT='/data/egrassi/snaketree/prj/align'
DIR_BASE='/data/egrassi/snaketree/prj/align/dataset/'

DIRS="170522","170529","170601","170707","170711","170714","170817","170825","170905"

GENOME="/data/egrassi/snaketree/task/sequences/dataset/iGenomes/mouse/mm10/genome"
CHR_SIZE="/data/egrassi/snaketree/task/sequences/dataset/iGenomes/mouse/mm10/chr.sizes"

TOPHAT_ENV=PRJ_ROOT+"/local/share/tophat_conda.yaml"
GTF="/data/egrassi/snaketree/task/sequences/dataset/iGenomes/mouse/mm10/tophat_gtf/genes"

ADAPTERS=PRJ_ROOT+"/local/share/data/illumina_primers_updated130717.fa"
# Downloaded the 25/05, subread 1.5.2 https://sourceforge.net/projects/subread/?source=typ_redirect
SUBREAD_GTF=PRJ_ROOT+"/local/share/data/mm10_RefSeq_exon.txt"

CORES ="5"
CORES_TRIM ="1"

