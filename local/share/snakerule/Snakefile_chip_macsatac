include: "snakemake"

# to call directly from other dirs
subworkflow otherworkflow:
    workdir: ALIGN_DIR
    snakefile: ALIGN_DIR+"/Snakefile"

rule all_filter:
    input: expand("{sample}.filtered2.bam", sample=THIS_SAMPLES)

# single mapping reads:
# [egrassi@gncomp3 align]$ time samtools view -F 0x4 RID100271.bam | grep -v "XS:i:" | cut -f1 | sort | uniq | wc -l

# remove duplicates:
# [egrassi@gncomp3 align]$ time samtools view -h -F 0x4 RID100271.bam | grep -v "XS:i:" | samtools view -h -b - | samtools rmdup --output-fmt SAM -s - - | grep -v "^@" |cut -f 1 |wc -l
# [bam_rmdupse_core] 654050 / 1933208 = 0.3383 in library '       '
# 1279158
#
# we need to remove duplicates, reads mapped to chrM
# then sort
# XS:i is a bowtie flag put for reads with secondary alignment, could generate problems for paired reads.
# changed a grep -v to an awk. use a library to process sam also for XS:i?
# rmdup with -s for single end, -S paired, could be used together
## I've read about doubts regarding duplicate removals (always PCR?) and peak calling quality. USe a filter on alignment quality?
## samtools view -b -q 10 foo.bam > foo.filtered.bam
rule filter_bam_dups:
    input: ALIGN_DIR+"/{sample}.bam"
    output: bam="{sample}.filtered.bam", duplog="{sample}.duplog"
    shell: "samtools view -h {input} | grep -v \"XS:i:\" | samtools view -h -b - | samtools sort | samtools rmdup -s - {output.bam} &> {output.duplog}"

rule filter_bam_chrM:
    input: "{sample}.filtered.bam"
    output: bam="{sample}.filtered2.bam"
    shell: "samtools view -h {input} | grep -v chrM | samtools view -h -b - > {output.bam}"


#conda: PYSAM_ENV
#[egrassi@gncomp3 macsatac]$ source activate pysam-new
# pysam has issues with the CENTOS bzip2, missing some libs (https://github.com/bioconda/bioconda-recipes/issues/5188)
# called manually in the end :(
# TODO FIXME (pysam-new) [egrassi@gncomp3 macsatac]$ python all_stats_atac.py 
#
rule all_stats:
    input: info_align=STATS
    output: "all_stats"
    script: "../../../local/src/all_stats_atac.py"

rule all_bw:
    input: expand("{sample}.bw", sample=THIS_SAMPLES)

# bigwig
rule bigwig:
    input: bam="{sample}.shift.bam", chrs=CHR_SIZE
    output: "{sample}.bw"
    shell:
        """
        TOT=`samtools flagstat {input.bam} | grep mapped | head -n 1 | tr " " "\t"| cut -f 1`; \\
        SCALE=`echo "scale=5;1000000.0/"$TOT | bc`; \\
        echo $SCALE > {output}.scale; \\
        samtools sort {input.bam} -o {input.bam}.tmp; \\
        genomeCoverageBed -scale $SCALE -bg -ibam {input.bam}.tmp -g {input.chrs} > {input.bam}.bedgraph.tmp
        sort -k1,1 -k2,2n {input.bam}.bedgraph.tmp > {input.bam}.bedgraph.sorted.tmp
        bedGraphToBigWig {input.bam}.bedgraph.sorted.tmp {input.chrs} {output}
        rm {input.bam}.bedgraph.tmp {input.bam}.tmp {input.bam}.bedgraph.sorted.tmp
        """

# single mapping reads:
# [egrassi@gncomp3 align]$ time samtools view -F 0x4 RID100271.bam | grep -v "XS:i:" | cut -f1 | sort | uniq | wc -l

# remove duplicates:
# [egrassi@gncomp3 align]$ time samtools view -h -F 0x4 RID100271.bam | grep -v "XS:i:" | samtools view -h -b - | samtools rmdup --output-fmt SAM -s - - | grep -v "^@" |cut -f 1 |wc -l
# [bam_rmdupse_core] 654050 / 1933208 = 0.3383 in library '       '
# 1279158

### atac and/or chipmentation only
# 5' shift
# clipping is not the best choice, these should not exists in theory XXX
# check if sorting is maintained, I do not see why it shouldn't
# Mantained:
#[ec2-user@ip-172-31-10-241 align]$ diff -q <(samtools view SRR5087528.bam | cut -f 1) <(samtools view SRR5087528.shift.bam | cut -f 1)
#[ec2-user@ip-172-31-10-241 align]$
# since macs should work also with bed we could avoid the last transformation maybe.
# passing to a bed loose cigar info, little bawking to check how may reads are removed due to clipping.
rule shift_reads:
    input: bam="{sample}.filtered.bam", chrlen=CHR_SIZE
    output: "{sample}.shift.bam"
    shell: "bedtools bamtobed -i {input.bam} | bedtools shift -g "+CHR_SIZE+ " -m "+SHIFT_MINUS+" -p "+SHIFT_PLUS+ " | bedtools bedtobam -g {input.chrlen} -i - > {output}"

# downsampling if needed
# samtools view -b -s 0.25 $$f > $$name2;
# shuf -n 5000000 -> headers get lost if done without caution
### macs
rule all_macs:
    input: ["macs/"+s1+"_peaks.broadPeak" for s1 in THIS_SAMPLES]

# -B/--bdg:
#fragment pileup, control lambda, -log10pvalue and -log10qvalue scores in bedGraph. 
# * -B --SPMR ask MACS2 to generate pileup signal file of 'fragment pileup per million reads' in bedGraph format.
rule macs_base:
    input: "{sample}.shift.bam"
    output: peak="macs/{sample}_peaks.broadPeak", pileup="macs/{sample}_treat_pileup.bdg"
    log: "{sample}.log"
    conda: MACS_ENV
    shell: "macs2 callpeak --broad --outdir=macs -t {input} --name={wildcards.sample} --format=BAM -g "+MACS_GENOME+" -B --SPMR &> {log}" 

#    5th: integer score for display
#    7th: fold-change
#    8th: -log10pvalue
#    9th: -log10qvalue
#    10th: relative summit position to peak start

rule all_macs_bw:
    input: expand("{sample}.macs.bw", sample=THIS_SAMPLES)

# filter out blacklisted regions here? TODO XXX
rule macs_bw:
    input: bdg="macs/{sample}_treat_pileup.bdg", chrlen=CHR_SIZE
    output: "{sample}.macs.bw"
    shell: 
        """
        sort -k1,1 -k2,2n {input.bdg} > {input.bdg}.tmp
        bedGraphToBigWig {input.bdg}.tmp {input.chrlen} {output}
        rm {input.bdg}.tmp
        """

rule all_filtered_peaks:
    input: expand("{sample}.filtered_peaks", sample=THIS_SAMPLES)

rule filter_peaks:
    input: peaks="macs/{sample}_peaks.broadPeak", blacklist=BLACKLIST
    output: "{sample}.filtered_peaks"
    shell:
        """
        sort -k8,8gr {input.peaks} | awk -F'\t' -v OFS='\t' '{{$4="Peak_"NR ; print $0}}' \
        | bedtools intersect -v -a stdin -b {input.blacklist} > {output}
        """

# downsampling if needed
# samtools view -b -s 0.25 $$f > $$name2;
# shuf -n 5000000 -> headers get lost if done without caution
#

# needs as fields: RID     READS_FILTERED  READS_ALIGNED   READS_MUL
# get from ../tophat/align_stats - ../align/align_stats and add w/o dup from sample.duplog (READS_NODUP)
# [egrassi@gncomp3 macsmentation]$ head -n1 ../align/align_stats 
# sample  tot     nalign  singlealign     mulalign
# [egrassi@gncomp3 macsmentation]$ head -n1 ../tophat/align_stats 
# RID     READS_FILTERED  READS_ALIGNED   READS_MUL
rule align_stats:
    input: "../align/align_stats"
    output: "align_stats"
    shell:
        """ 
            echo -e "RID\tREADS_FILTERED\tREADS_NALIGN\tREADS_ALIGNED\tREADS_MALIGN" > {output}
            sed 1d {input} >> {output}
        """

FIELDS={'IS_RPM': 'Y', 'GENOME': 'mm10', 'TRACK_DESCRIPTION': "b2-very-sensitive"}
RUN_INFO=PRJ_DIR+"/local/share/data/RNA-samples_170602.mine.tsv"
NEEDED_FIELDS=PRJ_DIR+"/local/share/data/wanted_fields_tracksdb"
include:PRJ_DIR+"/local/share/data/secrets.py"
rule add_track_build:
    input: bw="{sample}.bw", info_align="align_stats", info_run=RUN_INFO, needed_info=NEEDED_FIELDS, dup="{sample}.duplog"
    output: "{sample}.track_info.curl"
    run:
        dictio=FIELDS
        # fixed info for this run
        dictio['trackfile']=(input.bw, open(input.bw, 'rb'))
        # login info
        dictio['USR']=user
        dictio['PWD']=pwd
        dictio['debug']=1
        dictio['action']='new_sample_with_track'
        # load in dictionaries with info from xls
        with open(input.info_run, 'r') as info:
            header = info.readline().rstrip()
            fields = header.split("\t")
            rid_i = fields.index('RID')
            for l in info.readlines():
                l = l.rstrip()
                values = l.split("\t")
                if values[rid_i] == {wildcards.sample}.pop():
                    assert len({wildcards.sample}) == 1,  "Error in wildcard for sample"
                    # the {wildcards.sample} set is not emptied after the pop, copy semantics?
                    # our sample is in this line
                    for index, key in enumerate(fields):
                        assert key not in dictio, "Duplicated entry in " + str(input.info_run)
                        #if values[index] == '': ## we leave empty age and time post stimulus empty
                        #    values[index] = 'NA'
                        dictio[key] = values[index]
                    #print("got sample ", values[rid_i], "\n")
        dictio['SHORT_NAME'] = dictio['SHORT_NAME'].replace(" ", "_")
        with open(input.info_align, 'r') as info_align:
            # todo factorize ?
            header = info_align.readline().rstrip()
            fields = header.split("\t")
            for l in info_align.readlines():
                l = l.rstrip()
                values = l.split("\t")
                if values[0] == {wildcards.sample}.pop():
                    # our sample is in this line
                    dictio[fields[1]] = values[1]
                    dictio[fields[3]] = values[3]
                    dictio['READS_TOT'] = values[1]
        
        # compute GOOD using % align and total reads (XXX only total reads for now)
        if int(dictio['READS_FILTERED']) < 1000:
            dictio['IS_GOOD'] = 'N'
        else:
            dictio['IS_GOOD'] = 'Y'
        with open(input.dup, 'r') as info_align:
            dup = info_align.readline().rstrip()
            values = dup.split(" ")
            print(values[1])
            dictio['READS_NODUP'] = int(dictio['READS_ALIGNED']) - int(values[1])

        # Add NA for missing fields
        with open(input.needed_info, 'r') as needed:
            for l in needed.readlines():
                l = l.strip()
                if l not in dictio:
                    dictio[l] = 'NA'
        #READS_NODUP=" -F"READS_CHRM=" -F"STRAND=
        dictio['READS_CHRM'] = ''
        dictio['STRAND'] = ''
        # call curl
        curl_command = 'curl -F "trackfile=@' + input.bw + '" '
        for k in dictio.keys():
           if k != "trackfile":
               curl_command += "-F\"" + str(k) + "=" + str(dictio[k]) + "\" "
        curl_command += 'http://gncomp1.ich.techosp.it/tracksDB/remote.php;'
        print(curl_command)
        with open(output[0], 'w') as out:
            out.write(curl_command)
            out.write("\n")

rule add_track:
    input: "{sample}.track_info.curl"
    output: "{sample}.track_info.res"
    shell: "bash {input} &> {output}"

rule all_track:
    input: expand("{sample}.track_info.res", sample=THIS_SAMPLES)

# TODO CHECK:
#MD5_HASH: 0a4f6b63f7ef80d4f3ed1ac7fb012e30
#TRACK_FILE: SID100271-RID100271-TID24938882172715309-mm10-Elena_Grassi.bigWig
#TRACK_URL: http://gncomp1.ich.techosp.it/tracks/tracksDB/SID100271-RID100271-TID24938882172715309-mm10-Elena_Grassi.bigWig
#[egrassi@gncomp3 tophat]$ md5sum RID100271.bw
#0a4f6b63f7ef80d4f3ed1ac7fb012e30  RID100271.bw
#
#
rule add_track_macs_build:
    input: bw="{sample}.macs.bw", info_align="align_stats", info_run=RUN_INFO, needed_info=NEEDED_FIELDS, dup="{sample}.duplog"
    output: "{sample}.macs_track_info.curl"
    run:
        dictio=FIELDS
        # fixed info for this run
        dictio['trackfile']=(input.bw, open(input.bw, 'rb'))
        # login info
        dictio['USR']=user
        dictio['PWD']=pwd
        dictio['debug']=1
        dictio['action']='new_sample_with_track'
        # load in dictionaries with info from xls
        with open(input.info_run, 'r') as info:
            header = info.readline().rstrip()
            fields = header.split("\t")
            rid_i = fields.index('RID')
            for l in info.readlines():
                l = l.rstrip()
                values = l.split("\t")
                if values[rid_i] == {wildcards.sample}.pop():
                    assert len({wildcards.sample}) == 1,  "Error in wildcard for sample"
                    # the {wildcards.sample} set is not emptied after the pop, copy semantics?
                    # our sample is in this line
                    for index, key in enumerate(fields):
                        assert key not in dictio, "Duplicated entry in " + str(input.info_run)
                        #if values[index] == '': ## we leave empty age and time post stimulus empty
                        #    values[index] = 'NA'
                        dictio[key] = values[index]
                    #print("got sample ", values[rid_i], "\n")
        dictio['SHORT_NAME'] = dictio['SHORT_NAME'].replace(" ", "_")
        with open(input.info_align, 'r') as info_align:
            # todo factorize ?
            header = info_align.readline().rstrip()
            fields = header.split("\t")
            for l in info_align.readlines():
                l = l.rstrip()
                values = l.split("\t")
                if values[0] == {wildcards.sample}.pop():
                    # our sample is in this line
                    dictio[fields[1]] = values[1]
                    dictio[fields[3]] = values[3]
                    dictio['READS_TOT'] = values[1]
        
        # compute GOOD using % align and total reads (XXX only total reads for now)
        if int(dictio['READS_FILTERED']) < 1000:
            dictio['IS_GOOD'] = 'N'
        else:
            dictio['IS_GOOD'] = 'Y'
        with open(input.dup, 'r') as info_align:
            dup = info_align.readline().rstrip()
            values = dup.split(" ")
            print(values[1])
            dictio['READS_NODUP'] = int(dictio['READS_ALIGNED']) - int(values[1])
        dictio['TRACK_DESCRIPTION'] = 'macs'
        # Add NA for missing fields
        with open(input.needed_info, 'r') as needed:
            for l in needed.readlines():
                l = l.strip()
                if l not in dictio:
                    dictio[l] = 'NA'
        #READS_NODUP=" -F"READS_CHRM=" -F"STRAND=
        dictio['READS_CHRM'] = ''
        dictio['STRAND'] = ''
        # call curl
        curl_command = 'curl -F "trackfile=@' + input.bw + '" '
        for k in dictio.keys():
           if k != "trackfile":
               curl_command += "-F\"" + str(k) + "=" + str(dictio[k]) + "\" "
        curl_command += 'http://gncomp1.ich.techosp.it/tracksDB/remote.php;'
        print(curl_command)
        with open(output[0], 'w') as out:
            out.write(curl_command)
            out.write("\n")

rule add_track_macs:
    input: "{sample}.macs_track_info.curl"
    output: "{sample}.macs_track_info.res"
    shell: "bash {input} &> {output}"

rule all_track_macs:
    input: expand("{sample}.macs_track_info.res", sample=THIS_SAMPLES)
