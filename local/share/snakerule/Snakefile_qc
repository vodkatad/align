include: "snakemake"

# to call directly from other dirs
subworkflow otherworkflow:
    workdir: BASE_DIR
    snakefile: BASE_DIR+"/Snakefile"

rule all_fastqc:
    input: expand("fastqc_{sample}", sample=SAMPLES)

# filter out the low quality reads
# The first grep selects only valid reads:
# @<instrument-name>:<run ID>:<flowcell ID>:<lane>:<tile>:<x-pos>:<y-pos> " " <read number>:<is filtered>:<control number>:<barcode sequence>
# we keep reads with 'is filtered' == N. "Y if the read is filtered, N otherwise", answer to "did it fail the filtering step"?
# The second one removes lines composed only of '--': don't we risk breaking the 4 lines regularity?
# the -- are inserted by grep, possibly better to use --no-group-separator, but it does not work on all architectures.
# Then we substitute single spaces with _, we should have only the one in the id between the two portions.
# try a single perl oneliner for efficiency?
#rule filter_reads:
#    input: [BASE_DIR+"/{sample}.fastq.gz"]
#    output: "{sample}.fq"
#    benchmark: "{sample}.time"
#    shell: "zcat {input} | grep -A 3 '^@.* [^:]*:N:[^:]*:' | grep -v -- '^--$' | sed 's/ /_/g' > {output}"

# The filter is not needed:
#[egrassi@gncomp3 170522]$ zcat *fastq.gz |  grep -m 1 '^@.* [^:]*:Y:[^:]*:'
#[egrassi@gncomp3 170522]$

# fastqc
#threads: CORES
# -j/--cores in snakemake affects also rules to be executed in parallel? Am using CORES variable to differentiate.
# fastqc seems to run in parallel only when multiple fastq as arguments.
rule fastqc:
    input: otherworkflow([BASE_DIR+"/{sample}.fastq.gz"])
    output: "fastqc_{sample}"
    shell: "mkdir -p {output}; fastqc --extract -t " + CORES + " -o {output} {input}; rm {output}/{wildcards.sample}_fastqc.zip"
 
# trimming
# adapter trimming is done by bcl2fastq when demultiplexing - do we need to trim reads considering quality?
