
######## Snakemake header ########
import sys; sys.path.insert(0, "~/miniconda3/envs/pysam-new/lib/python3.6/site-packages/"); import pickle; snakemake = pickle.loads(b'\x80\x03csnakemake.script\nSnakemake\nq\x00)\x81q\x01}q\x02(X\x05\x00\x00\x00inputq\x03csnakemake.io\nInputFiles\nq\x04)\x81q\x05XI\x00\x00\x00/data/egrassi/snaketree/prj/align/dataset/171009//align/align_stats_namesq\x06a}q\x07(X\x06\x00\x00\x00_namesq\x08}q\tX\n\x00\x00\x00info_alignq\nK\x00N\x86q\x0bsh\nh\x06ubX\x06\x00\x00\x00outputq\x0ccsnakemake.io\nOutputFiles\nq\r)\x81q\x0eX\t\x00\x00\x00all_statsq\x0fa}q\x10h\x08}q\x11sbX\x06\x00\x00\x00paramsq\x12csnakemake.io\nParams\nq\x13)\x81q\x14}q\x15h\x08}q\x16sbX\t\x00\x00\x00wildcardsq\x17csnakemake.io\nWildcards\nq\x18)\x81q\x19}q\x1ah\x08}q\x1bsbX\x07\x00\x00\x00threadsq\x1cK\x01X\t\x00\x00\x00resourcesq\x1dcsnakemake.io\nResources\nq\x1e)\x81q\x1f(K\x01K\x01e}q (h\x08}q!(X\x06\x00\x00\x00_coresq"K\x00N\x86q#X\x06\x00\x00\x00_nodesq$K\x01N\x86q%uh"K\x01h$K\x01ubX\x03\x00\x00\x00logq&csnakemake.io\nLog\nq\')\x81q(}q)h\x08}q*sbX\x06\x00\x00\x00configq+}q,X\x04\x00\x00\x00ruleq-X\t\x00\x00\x00all_statsq.ub.')
######## Original script #########
#print(snakemake.input)
ALIGN_DIR="/data/egrassi/snaketree/prj/align/dataset/171009/align"
import pysam
with open(snakemake.output[0], 'w') as out:
    with open(snakemake.input.info_align, 'r') as info_align:
        for l in info_align.readlines():
            res = ""
            l = l.rstrip()
            values = l.split("\t")
            # our sample is in this line
            # rid name  tot     nalign  singlealign     mulalign        align
            #dictio[values[0]] = values[1:] # reads filtered and tot now will be different.. READS_TOT needs to be loaded from {sample}.trimming.short
            rid = values[0]
            res += "\t".join(values)
            #str.join(sequence)
            with open(ALIGN_DIR+"/"+rid+".trimming.short", 'r') as info_align:
                done = False
                values = []
                while not done:
                    dup = info_align.readline().rstrip()
                    values = dup.split(" ")
                    if len(values) > 2 and values[1] == "Reads:":
                        done = True
                res += "\t" + values[2]
            # added initial reads, still need after rmdup and chrm removal
            with open(rid+".duplog", 'r') as info_dup:
                dup = info_dup.readline().rstrip()
                values = dup.split(" ")
                res += "\t" + values[1]
                #dictio[values[0]].append(int(values[1])) # this is the number of duplicates found
            flagstat = pysam.flagstat(rid+".filtered2.bam")
            #dictio[values[0]].append(int(flagstat.split('\n')[4].split(" ")[0]))
            res += "\t" + flagstat.split('\n')[4].split(" ")[0] # total reads after all filters
            out.write(res)
            out.write("\n")

