library(ggplot2)
library(ggsignif)
all_f <- snakemake@input[["all_de"]]
lps_induced_f <- snakemake@input[["lps"]]
gc_f <- snakemake@input[["gc"]]
out_tsv <- snakemake@output[["tsv"]]
thr <- as.numeric(snakemake@params[["thr"]])

get_sign <- function(target, thr, all) {
  all[all$name==target & all$padj_multi <thr,"entrez"]
}

all <- read.table(all_f, sep="\t", header=TRUE)
lps <- read.table(lps_induced_f, sep="\t", header=FALSE)
entrez_lps <- unlist(lapply(strsplit(as.character(lps$V1), "_", fixed=T), function(x) {x[1]}))
gc <- read.table(gc_f, sep="\t", header=FALSE)
colnames(gc) <- c("entrez", "GC")
#gc[gc$entrez %in% entrez_lps, 'lps'] <- "yes"
#colnames(gc) <- c("entrez", "GC", "lps")
induced <- gc[gc$entrez %in% entrez_lps,]

wilcox_ggplot <- function(target, all, thr, gc) {
  sign <- get_sign(target, thr, all)
  gc$de <- "no"
  gc[gc$entrez %in% sign,'de'] <- "yes"
  gc1 <- gc[gc$de=="yes","GC"]
  gc2 <- gc[gc$de=="no","GC"]
  w <- wilcox.test(gc1, gc2)
  m1 <- median(gc1)
  m2 <- median(gc2)
  #ggplot(gc, aes(GC, colour = de)) +geom_density()
  #ggplot(gc, aes(y=GC, x=de, color=de)) + geom_violin(colour = "#3366FF")+theme_bw()+ geom_jitter(height = 0, width = 0.1)+scale_color_manual(values=c("dark green","gold"))
  ggplot(gc, aes(y=GC, x=de, fill=de)) + geom_violin()+geom_signif(comparisons = list(c("no","yes")))+geom_jitter(height = 0, width = 0.1)+scale_fill_manual(values=c("dark green","gold"))+theme_bw()
  ggsave(paste(c(target, thr, "pdf"), collapse ='.'))
  c(w$p.value, m1, m2)
}

res <- as.data.frame(t(sapply(levels(all$name), wilcox_ggplot, all, thr, induced)))
colnames(res) <- c("pvalue","median_de", "median_nde")
res$name <- rownames(res)
write.table(res, file=out_tsv, sep="\t", quote=FALSE, row.names = FALSE)
save.image("wip.RData")
