---
title: "Align report"
author:
    - "Elena Grassi"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
  highlight: tango
  number_sections: no
  theme: default
  toc: yes
  toc_depth: 3
  toc_float:
  collapsed: no
  smooth_scroll: yes
params:
  stats: placeholder
  wd: placeholder
---
```{r setup, include=FALSE}
knitr::opts_knit$set(root.dir = normalizePath(params$wd))
```

## Aligned reads percentages and classes of reads ##
```{r alignperc, echo=FALSE}
cat(params$wd)
library(ggplot2)
align <- read.table(params$stats, sep="\t", header=TRUE, row=1)
data <- data.frame(((perc=align$singlealign+align$mulalign)/align$tot)*100)
colnames(data) <- "perc"
ggplot(data, aes(x=perc)) + geom_histogram(binwidth=.8, colour="black", fill="white") + geom_vline(aes(xintercept=mean(perc)), color="red", linetype="dashed", size=1)+xlab("% mapped reads")
align2 <- align
align2$tot <- NULL
align2$align <- NULL
library(reshape)
align2$id <- rownames(align2)
mdata <- melt(align2, measure.vars=c("nalign", "singlealign","mulalign"))
colnames(mdata) <- c("sample","reads","n")
ggplot(data=mdata, aes(x=reorder(sample,n), y=n, fill=reads, width=.75)) +
geom_bar(stat="identity")+xlab("sample")+theme(axis.text.x=element_text(angle=75, hjust=1, size=15))
```

```{r check, echo=FALSE}
ok <- all(align$tot == (align$nalign+align$singlealign+align$mulalign))
```

The alignment file stats looks good (tot == nalign+single+mul) ? `r ok`
