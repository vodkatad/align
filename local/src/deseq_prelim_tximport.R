#print(snakemake@params[["threads"]]) # rid to name
library(DESeq2)
library(tximport)
library("BiocParallel")
register(MulticoreParam(as.numeric(snakemake@params[["threads"]])))
#print(snakemake@input[[1]]) # rid to name
#print(snakemake@input[[2]]) # samples to be removed
#print(snakemake@input[[3]]) # counts 1
#print(snakemake@input[[4]]) # counts 2 ... maybe more
mincounts <- as.numeric(snakemake@params[["mincounts"]]) # more strict filtering on expression
mingroup <- as.numeric(snakemake@params[["mingroup"]])

names <- read.table(snakemake@input[[1]], sep="\t", header=TRUE)

save.image("wip.Rdata")
files_list <- read.table(snakemake@input[[3]],  comment.char="", stringsAsFactors = FALSE, header=FALSE)
files <- files_list[,1]
names(files) <- snakemake@params[["samples"]]

if (!all(names$RID==names(files))) {
    stop("RIDs in the annotation does not match those in count files")
}


fake <- read.table(files[1], sep="\t", header=TRUE, comment.char="")
txg_fake <- data.frame(genes=fake[,1], tx=fake[,1])
txi <- tximport(files, type="kallisto",  geneIdCol="target_id", abundanceCol="tpm", lengthCol="eff_length", countsCol="est_counts", tx2gene=txg_fake)

#horrible
reps <- c(rep("1", length(files)/2), rep("2", length(files)/2))
conditions <- data.frame(condition_sample=names$SHORT_NAME, row.names=names$RID, rep=reps)
conditions$comparison <- sapply(strsplit(as.character(conditions$condition_sample), " "), function(x) {if (length(x) == 1) {x[1]} else { x[2]} })

tbr <- read.table(snakemake@input[[2]], sep="\t", header=FALSE)
samples_to_remove <- tbr[,1]
#TODO if needed
#merged_pruned <- merged[, !colnames(merged) %in% samples_to_remove]
#conditions_pruned <- conditions[!rownames(conditions) %in% samples_to_remove,]

conditions$comparison <- gsub("luc", "ctrl", conditions$comparison, fixed=TRUE)
conditions$comparison <- gsub("SHC00.", "ctrl", conditions$comparison)
conditions$comparison <- as.factor(conditions$comparison)

#dds <- DESeqDataSetFromMatrix(countData = merged_pruned, colData = conditions_pruned, design = ~ rep + comparison)
dds <- DESeqDataSetFromTximport(txi, colData = conditions, design = ~ rep + comparison)
filterGenes <- rowSums(counts(dds) > mincounts) < mingroup
dds <- dds[!filterGenes]
#dds <- dds[ rowSums(counts(dds)) > 1, ]
dds <- DESeq(dds, parallel=TRUE, betaPrior=TRUE)

save.image(snakemake@output[[1]])
