#print(snakemake@params[["threads"]]) # rid to name
library(DESeq)

library("BiocParallel")
register(MulticoreParam(as.numeric(snakemake@params[["threads"]])))
mincounts <- as.numeric(snakemake@params[["mincounts"]]) # more strict filtering on expression
mingroup <- as.numeric(snakemake@params[["mingroup"]])

data <- read.table(snakemake@input[[3]],  comment.char="#", stringsAsFactors = FALSE, header=TRUE, row =1)
counts <- data[, seq(6, ncol(data))]
colnames(counts) <- gsub(".bam", "", colnames(counts), fixed=TRUE)

names <- read.table(snakemake@input[[1]], sep="\t", header=TRUE)
if (!all(names$SID==colnames(counts))) {
    stop("SIDs in the annotation does not match those in count files")
}
conditions <- data.frame(comparison=names$SHORT_NAME, row.names=names$SID)

tbr <- read.table(snakemake@input[[2]], sep="\t", header=FALSE)
samples_to_remove <- tbr[,1]
counts_pruned <- counts[, !colnames(counts) %in% samples_to_remove]
conditions_pruned <- conditions[!rownames(conditions) %in% samples_to_remove,,drop=FALSE]

dds <- newCountDataSet(counts_pruned, conditions_pruned)
#
dds <- DESeqDataSetFromMatrix(countData = counts_pruned, colData = conditions_pruned, design = ~ comparison)
filterGenes <- rowSums(counts(dds) > mincounts) < mingroup
dds <- dds[!filterGenes]
#dds <- dds[ rowSums(counts(dds)) > 1, ]

#dds <- DESeq(dds, parallel=TRUE, betaPrior=TRUE)
dds <- estimateSizeFactors(dds)
sf <- sizeFactors(dds)
sizeFactors(dds) <- rep(1, length(sf))
dds <- estimateDispersions(dds) # parallel=TRUE ?
dds <- nbinomWaldTest(dds, betaPrior=TRUE)

save.image(snakemake@output[[1]])
